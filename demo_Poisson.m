%% Solving the Poisson equation on triangle and Cartesian meshes
%%%
%%% -Delta u = f on Omega = [0,1]x[0,1]
%%% 
%%% with Dirichlet boundary conditions

addpath Square

f = @(x,y) cos(pi*(x-0.5)).*cos(pi*(y-0.5)); % Source term
u = @(x,y) f(x,y)/(2*pi*pi); % Exact solution term

Nmesh = 8;

%% Triangle meshes
hTri = zeros(1,Nmesh);
errorTri = zeros(1,Nmesh);
for i = 1:Nmesh
    
    % Load triangular mesh
    load(['Square_', num2str(i)])
    
    %%% Number of control volumes, their area and centers
    nbtri = size(FVmsh.ContVols,2); 
    mK = FVmsh.ContVolsInfo(3,:);
    xK = FVmsh.ContVolsInfo(1,:);
    yK = FVmsh.ContVolsInfo(2,:);

    %%% Edges length, intercenter distances and transmissivities 
    msig = FVmsh.EdgesInfo(1,:);
    dsig = FVmsh.EdgesInfo(2,:);
    tausig=msig./dsig;

    %%% Indices of inside and boundary edges
    inside=find(FVmsh.Edges(4,:)>0); 

    %%% Discrete Dirichlet Finite Volume Laplacian Matrix
    LAPMAT= sparse([FVmsh.Edges(3,:), FVmsh.Edges(4,inside), FVmsh.Edges(3,inside), FVmsh.Edges(4,inside)],...
        [FVmsh.Edges(3,:), FVmsh.Edges(4,inside), FVmsh.Edges(4,inside), FVmsh.Edges(3,inside)],...
        [-tausig, -tausig(inside), tausig(inside), tausig(inside)],nbtri,nbtri);

    %%% Solving the Poisson equation
    F = mK'.*f(xK, yK)';
    U = -LAPMAT\F;
    Uex = u(xK, yK)';

    %%% Relative supremum norm error
    hTri(i) = FVmsh.Size;
    errorTri(i) = norm(Uex - U, 'inf')/norm(Uex,'inf');
end



%% Plot of the solution
% Comment if you don(t have the PDEtoolbox

figure
pdesurf(FVmsh.Vertices,FVmsh.ContVols,pdeprtni(FVmsh.Vertices,FVmsh.ContVols,U'));
colormap(jet)
colorbar
xlabel('x')
ylabel('y')
view(0,90)



%% Cartesian meshes
hCart = zeros(1,Nmesh);
errorCart = zeros(1,Nmesh);
for i = 1:Nmesh
    
    Nx = 4*2^(i-1);
    Ny = 4*2^(i-1);
    [FEmsh, FVmsh] = CartFVMesh(0,1,0,1, Nx, Ny);
    
    
    %%% Number of control volumes, their area and centers
    nbtri = size(FVmsh.ContVols,2); 
    mK = FVmsh.ContVolsInfo(3,:);
    xK = FVmsh.ContVolsInfo(1,:);
    yK = FVmsh.ContVolsInfo(2,:);

    %%% Edges length, intercenter distances and transmissivities 
    msig = FVmsh.EdgesInfo(1,:);
    dsig = FVmsh.EdgesInfo(2,:);
    tausig=msig./dsig;

    %%% Indices of inside and boundary edges

    inside=find(FVmsh.Edges(4,:)>0); 

    %%% Discrete Dirichlet Finite Volume Laplacian Matrix
    LAPMAT= sparse([FVmsh.Edges(3,:), FVmsh.Edges(4,inside), FVmsh.Edges(3,inside), FVmsh.Edges(4,inside)],...
        [FVmsh.Edges(3,:), FVmsh.Edges(4,inside), FVmsh.Edges(4,inside), FVmsh.Edges(3,inside)],...
        [-tausig, -tausig(inside), tausig(inside), tausig(inside)],nbtri,nbtri);

    %%% Solving the Poisson equation
    F = mK'.*f(xK, yK)';
    U = -LAPMAT\F;
    Uex = u(xK, yK)';

    %%% Relative supremum norm error
    hCart(i) = FVmsh.Size;
    errorCart(i) = norm(Uex - U, 'inf')/norm(Uex,'inf');
end

%% Convergence curves

figure
loglog(hTri,errorTri,'+-')
xlabel('Mesh size')
ylabel('Relative sup norm error')
title('Convergence plot for the Poisson equation')
hold on
loglog(hCart,errorCart,'+-')
legend('Triangles','Cartesian')

