# FVmeshes

FVmeshes is a Matlab toolbox to create, convert and use 2D meshes to implement two-point flux approximation Finite Volume schemes and generalizations. The basic mathematical notions on Finite Volume schemes can be found [here](https://www.i2m.univ-amu.fr/perso/raphaele.herbin/PUBLI/bookevol.pdf).  Below we describe the data structures of the meshes produced with FVmeshes. 

## The mesh structures

FVmeshes stores meshes in two Matlab structures : FEmsh and FVmsh

### FEmsh

FEmsh.Vertices: Matrix containing the vertices in the mesh (one vertex = one column)
- Line 1: x coordinate
- Line 2: y coordinate

FEmsh.BoundaryEdges: Matrix containing the boundary edges of the mesh (one edge = one column)
- Line 1: Endpoint 1 index (in FEmsh.Vertices)
- Line 2: Endpoint 2 index (in FEmsh.Vertices)

FEmsh.MaxNumEdges: Maximal number of edges of an element

FEmsh.Elements: Matrix containing the elements (one column = one element) 
- Line 1: Vertex 1 index (in FEmsh.Vertices)
- Line 2: Vertex 2 index (in FEmsh.Vertices)
- ...
- Line N: Vertex N index (in FEmsh.Vertices)
- Line N+1: Index of the domain in which the element is (0 by default)

### FVmsh

FVmsh.Vertices: Same as FEmsh.Vertices

FVmsh.MaxNumEdges: Maximal number of edges of a control volume 

FVmsh.ContVols: Matrix containing the N-edges control volumes of the mesh (one column = one control volume). The vertices are reordered in trigonometric order.
- Line 1: Vertex 1 index (in FVmsh.Vertices)
- Line 2: Vertex 2 index (in FVmsh.Vertices)
- ...
- Line N: Vertex N index (in FVmsh.Vertices)
- Line N+1: Index of the domain in which the control volume is (0 by default)

FVmsh.ContVolsInfo: Matrix containing additional geometric information on the control volumes
- Line 1: x coordinate of the center of the control volume (may not be barycenter)
- Line 2: y coordinate of the center of the control volume (may not be barycenter)
- Line 3: Area of the control volume
- Line 4: x coordinate of the barycenter of the control volume
- Line 5: y coordinate of the barycenter of the control volume
- Line 6: Diameter of the control volume



FVmsh.Edges: Matrix containing the edges of the mesh (one edge = one column). 
Here is a schematic of an edge (vertical line) W and E represent the neighboring triangles and S and N are the vertices at endpoints of the edge. By convention is always inside the domain and E does not exist if the edge is on the boundary.
```
            N
            |
            |
            |
  W ------------------ E
            |
            |
            |
            S
```
- Line 1: Index (in FEmsh.Vertices) of the South point
- Line 2: Index (in FEmsh.Vertices) of the North point
- Line 3: Index (in FEmsh.Elements / FVmsh.ContVols) of the West point
- Line 4: Index (in FEmsh.Elements / FVmsh.ContVols) of the East point. Set to 0 if the edge is on the boundary.
- Line 5: Part of the boundary in which the edge is (0 by default)

FVmsh.EdgesInfo: Matrix containing additional information on the edges.
- Line 1: Length of the edge (South-North)
- Line 2: Distance between the neighboring cell centers (East-West). If the edge is on the outside the distance is between West cell center and the edge barycenter.
- Line 3: Area of the West diamond (triangle between edge and West cell center).
- Line 4: Area of the East diamond (triangle between edge and East cell center). 0 if the edge is on the boundary.
- Line 5: x coordinate of the normal vector to the edge pointing to the East.
- Line 6: y coordinate of the normal vector to the edge pointing to the East.
- Line 7: x coordinate of the edge barycenter
- Line 8: y coordinate of the edge barycenter

FVmsh.Size: Maximum diameter of triangles

FVmsh.Orthogonal: Boolean saying whether SN is orthogonal to WE for all  control volumes.

FVmsh.Admissible: Boolean saying whether the normal to SN pointing to the East is in the same direction as WE. For triangle meshes it may fail if circumcenters are far from triangles (i.e. take two neighboring flattened triangles). Triangle meshes should be admissible if they are produced with the Frontal Delaunay algorithm.

FVmsh.WhichNotAdmissible: Contains the list of non-admissible edges (such as the normal to SN is not in the same direction as WE). Contains 0 if admissible.

## The Matlab functions and scripts in this repository

The repository is composed of the following Matlab functions

- GmshToVF2D: converts a triangular 2D mesh of gmsh into matlab structures containing the info needed to implement a TPFA finite volume scheme in 2D.
```
 [FVmsh, FEmsh] = GmshtoFVTri2D(gmshSavefile)
```
- FEtoFVtri: Conversion of a triangular "finite element" mesh (such as those of gmsh or matlab pdetoolbox) into a "finite volume" mesh (matlab structures containing the info needed to implement a TPFA finite volume scheme in 2D.
```
FVmsh=FEtoFVtri(FEmsh)
```
- plotFVmesh: Plots the edges of the finite volume mesh 
```
plotFVmesh(FVmsh)
```
- [FEmsh, FVmsh] = CartFVMesh(xmin, xmax, ymin, ymax, Nx, Ny, varargin)
CartFVMesh: Creates of a 2D quadrangular Cartesian "finite element" mesh (with data similar to that of matlab pdetoolbox) and "finite volume" mesh (matlab structures containing the info needed to implement a TPFA finite volume scheme in 2D.
```
[FEmsh, FVmsh] = CartesianEF(xmin, xmax, ymin, ymax, Nx, Ny)
```
- demo_Poisson: Solves the Poisson equation using the FVmeshes structures and plots the convergence curve and solutions. (Essentially Tutorial 3)

## Tutorial 1: Create a mesh on gmsh and convert it into FVmeshes Matlab structures

Let us create a triangular mesh of a rectangle $[0,1]\times[0,2]$ with [gmsh](https://gmsh.info/). 

### Create the geometry in a .geo file

Create the file Rectangle.geo containing the following lines.

```
// Mesh size at points
s1 = 1e-1;

// Defining points
Point(1) = {0, 0, 0, s1};
Point(2) = {0, 2, 0, s1};
Point(3) = {1, 2, 0, s1};
Point(4) = {1, 0, 0, s1};

// Defining lines
Line(1) = {1,2};
Line(2) = {2,3};
Line(3) = {3,4};
Line(4) = {4,1};

// Defining boundary (Square)
Curve Loop(1) = {1,2,3,4};

// Defining Surface
Plane Surface(1) = {1};

// Matlab format for saving
Mesh.Format = 50;

// Frontal-Delaunay Algorithm
Mesh.Algorithm=6; 
```
It is important to choose the Frontal-Delaunay algorithm (Mesh.Algorithm=6) to ensure the creation of an admissible mesh. It is mandatory to choose the Matlab format (Mesh.Format = 50) for saving the mesh.

### Run gmsh to make the mesh

Run in a terminal (on linux)

```
gmsh -2 Rectangle.geo
```
This creates a file named Rectangle.m which contains the mesh

### Use FVmeshes to convert the mesh in a usable finite volume mesh

Open Matlab and run the following command
```
[FVmsh, FEmsh] = GmshtoFVTri2D(filename);
```
This creates the FVmsh and FEmsh from the Rectangle.m

### Visualize the mesh

Run the following Matlab command to visualize the mesh.
```
plotFVmesh(FVmsh);
```
If the mesh is not admissible black marks will appear in the control volumes which do not satisfy the admissiblity condition.

## Tutorial 2: Create a Cartesian mesh 

To create a Cartesian mesh of $[0,1]\times[0,2]$ run the Matlab command to visualize the mesh.
```
Nx = 200;
Ny = 300;
[FEmsh, FVmsh] = CartesianEF(0,1,0,2,Nx,Ny);
```
Then the mesh can be visualized as before using the command
```
plotFVmesh(FVmsh);
```

## Tutorial 3: Use FVmeshes to solve a PDE
 
 Let us solve the Poisson equation $-\Delta u = f$ on a rectangle $[0,1]\times[0,1]$ meshed with a sequence of Triangular and Cartesian meshes created with Tutorial 1 and Tutorial 2. The triangular meshes are available on the repositories. We endow the equation with homogenenous Dirichlet boundary conditions and take $f(x,y) = \cos(\pi (x-0.5)) \cos(0.5\pi (y-1))$. Let us start with the triangular meshes by running in Matlab
```
f = @(x,y) cos(pi*(x-0.5)).*cos(pi*(y-0.5));
u = @(x,y) f(x,y)/(2*pi*pi);

Nmesh = 8;
hTri = zeros(1,Nmesh);
errorTri = zeros(1,Nmesh);
for i = 1:Nmesh
    
    load(['Square_', num2str(i)])
    
    %%% Number of control volumes, their area and centers
    nbtri = size(FVmsh.ContVols,2); 
    mK = FVmsh.ContVolsInfo(3,:);
    xK = FVmsh.ContVolsInfo(1,:);
    yK = FVmsh.ContVolsInfo(2,:);

    %%% Edges length, intercenter distances and transmissivities 
    msig = FVmsh.EdgesInfo(1,:);
    dsig = FVmsh.EdgesInfo(2,:);
    tausig=msig./dsig;

    %%% Indices of inside and boundary edges

    inside=find(FVmsh.Edges(4,:)>0); 

    %%% Discrete Dirichlet Finite Volume Laplacian Matrix
    LAPMAT= sparse([FVmsh.Edges(3,:), FVmsh.Edges(4,inside), FVmsh.Edges(3,inside), FVmsh.Edges(4,inside)],...
        [FVmsh.Edges(3,:), FVmsh.Edges(4,inside), FVmsh.Edges(4,inside), FVmsh.Edges(3,inside)],...
        [-tausig, -tausig(inside), tausig(inside), tausig(inside)],nbtri,nbtri);

    %%% Solving the Poisson equation
    F = mK'.*f(xK, yK)';
    U = -LAPMAT\F;
    Uex = u(xK, yK)';

    %%% Relative supremum norm error
    hTri(i) = FVmsh.Size;
    errorTri(i) = norm(Uex - U, 'inf')/norm(Uex,'inf');
end
```
Then we can do the same for Cartesian meshes
```
hCart = zeros(1,Nmesh);
errorCart = zeros(1,Nmesh);

for i = 1:Nmesh
    
    Nx = 4*2^(i-1);
    Ny = 4*2^(i-1);
    [FEmsh, FVmsh] = CartFVMesh(0,1,0,1, Nx, Ny);
    
    
    %%% Number of control volumes, their area and centers
    nbtri = size(FVmsh.ContVols,2); 
    mK = FVmsh.ContVolsInfo(3,:);
    xK = FVmsh.ContVolsInfo(1,:);
    yK = FVmsh.ContVolsInfo(2,:);

    %%% Edges length, intercenter distances and transmissivities 
    msig = FVmsh.EdgesInfo(1,:);
    dsig = FVmsh.EdgesInfo(2,:);
    tausig=msig./dsig;

    %%% Indices of inside and boundary edges

    inside=find(FVmsh.Edges(4,:)>0); 

    %%% Discrete Dirichlet Finite Volume Laplacian Matrix
    LAPMAT= sparse([FVmsh.Edges(3,:), FVmsh.Edges(4,inside), FVmsh.Edges(3,inside), FVmsh.Edges(4,inside)],...
        [FVmsh.Edges(3,:), FVmsh.Edges(4,inside), FVmsh.Edges(4,inside), FVmsh.Edges(3,inside)],...
        [-tausig, -tausig(inside), tausig(inside), tausig(inside)],nbtri,nbtri);

    %%% Solving the Poisson equation
    F = mK'.*f(xK, yK)';
    U = -LAPMAT\F;
    Uex = u(xK, yK)';

    %%% Relative supremum norm error
    hCart(i) = FVmsh.Size;
    errorCart(i) = norm(Uex - U, 'inf')/norm(Uex,'inf');
end
```
Now we can plot and compare the convergence curves of the finite volume method on both sequences of mesh.
```
figure
loglog(hTri,errorTri,'+-')
hold on
loglog(hCart,errorCart,'+-')
xlabel('Mesh size')
ylabel('Relative sup norm error')
title('Convergence plot for the Poisson equation')
legend('Triangles','Cartesian')
```

## Authors and acknowledgment

Author: Maxime Herda (maxime.herda@inria.fr)

Thanks to Claire Chainais-Hillairet who wrote the initial version of the FEtoFVtri function and to Clément Cancès for discusssions on the topic.

## License

GNU General Public License v3.0

## Project status

This has been created for personnal use. Thus the code may not be optimized nor designed to be user-friendly. There might or might not be updates in the future.
