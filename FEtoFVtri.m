function FVmsh=FEtoFVtri(FEmsh)
% FEtoFVtri: Conversion of a triangular "finite element" mesh (such as 
%            those of gmsh or matlab pdetoolbox) into a "finite volume"
%            mesh (matlab structures containing the info needed to 
%            implement a TPFA finite volume scheme in 2D.
% 
% FVmsh=FEtoFVtri(FEmsh)
%
%%%%%%%%%
% INPUT
%%%%%%%%%
%
% FEmsh: matlab structure containing the Finite Element mesh. The fields of 
% FEmsh are the following.
%
% FEmsh.Vertices: Matrix containing the vertices in the mesh 
% (one vertex = one column)
%   Line 1: x coordinate
%   Line 2: y coordinate
%
% FEmsh.BoundaryEdges: Matrix containing the boundary edges of the mesh
% (one edge = one column)
%   Line 1: Endpoint 1 index (in FEmsh.Vertices)
%   Line 2: Endpoint 2 index (in FEmsh.Vertices)
%
% FEmsh.MaxNumEdges: Maximal number of edges of an element. 
%
% FEmsh.Elements: Matrix containing the elements 
% (one column = one element). 
%   Line 1: Vertex 1 index (in FEmsh.Vertices)
%   Line 2: Vertex 2 index (in FEmsh.Vertices)
%   ...
%   Line N: Vertex N index (in FEmsh.Vertices)
%   Line N+1: Index of the domain in which the element is (0 by default)
%
%%%%%%%%%
% OUTPUT
%%%%%%%%%
%
% FVmsh: Matlab structure containing the Finite Volume mesh. The fields of 
% FVmsh are the following.
%
% FVmsh.Vertices: Same as FEmsh.Vertices
%
% FVmsh.MaxNumEdges: Maximal number of edges of a control volume. 
%
% FVmsh.ContVols: Matrix containing the N-edges control volumes 
%                 of the mesh (one column = one control volume). The 
%                 vertices are reordered   in trigonometric order.
%   Line 1: Vertex 1 index (in FVmsh.Vertices)
%   Line 2: Vertex 2 index (in FVmsh.Vertices)
%   ...
%   Line N: Vertex N index (in FVmsh.Vertices)
%   Line N+1: Index of the domain in which the control volume is 
%             (0 by default)
%
% FVmsh.ContVolsInfo: Matrix containing additional geometric information 
%                     on the control volumes
%   Line 1: x coordinate of the center of the control volume (may not be
%           barycenter)
%   Line 2: y coordinate of the center of the control volume (may not be
%           barycenter)
%   Line 3: Area of the control volume
%   Line 4: x coordinate of the barycenter of the control volume
%   Line 5: y coordinate of the barycenter of the control volume
%   Line 6: Diameter of the control volume
%
%
%
% FVmsh.Edges: Matrix containing the edges of the mesh
%   (one edge = one column). Here is a schematic of an edge (vertical line)
%   W and E represent the neighboring triangles and S and N are the 
%   vertices at endpoints of the edge. By convention W is always inside the
%   domain and E does not exist if the edge is on the boundary.
%             N
%             |
%             |
%             |
%   W ------------------ E
%             |
%             |
%             |
%             S
%   Line 1: Index (in FEmsh.Vertices) of the South point
%   Line 2: Index (in FEmsh.Vertices) of the North point
%   Line 3: Index (in FEmsh.Elements / FVmsh.ContVols) of the West point
%   Line 4: Index (in FEmsh.Elements / FVmsh.ContVols) of the East point.
%           Set to 0 if the edge is on the boundary.
%   Line 5: Part of the boundary in which the edge is (0 by default)
%
% FVmsh.EdgesInfo: Matrix containing additional information on the edges.
%   Line 1: Length of the edge (South-North)
%   Line 2: Distance between the neighboring cell centers (East-West). If 
%           the edge is on the outside the distance is between West cell 
%           center and the edge barycenter.
%   Line 3: Area of the West diamond (triangle between edge and West cell
%           center).
%   Line 4: Area of the East diamond (triangle between edge and East cell
%           center). 0 if the edge is on the boundary.
%   Line 5: x coordinate of the normal vector to the edge pointing to the
%           East
%   Line 6: y coordinate of the normal vector to the edge pointing to the 
%           East
%   Line 7: x coordinate of the edge barycenter
%   Line 8: y coordinate of the edge barycenter
%
% FVmsh.Size: Maximum diameter of triangles
%
% FVmsh.Orthogonal: Boolean saying whether SN is orthogonal to WE for all
%                   control volumes.
%
% FVmsh.Admissible: Boolean saying whether the normal to SN pointing to the
%                   East is in the same direction as WE. For triangle 
%                   meshes it may fail if circumcenters are far from 
%                   triangles (i.e. take two neighboring flattened 
%                   triangles). Triangle meshes should be admissible if 
%                   they are produced with the Frontal Delaunay algorithm.
%
% FVmsh.WhichNotAdmissible: Contains the list of non-admissible edges (such
%                           as the normal to SN is not in the same 
%                           direction as WE). Contains 0 if admissible.


addpath textprogressbar

%% Extracting data from FEmsh and initialising

disp('-------------------------------')
disp('Initializing creation of FV mesh...')

p = FEmsh.Vertices;
t = FEmsh.Elements;

nbtri=size(t,2);

% Initialising
FVmsh = struct;
FVmsh.Vertices = FEmsh.Vertices;
FVmsh.MaxNumEdges = 3;

%% Elements and ElementsInfo Matrices

% Vertices of triangles (ABC) in the mesh

disp('Computing elements geometrical information...')

A = p(:,t(1,:)); 
B = p(:,t(2,:)); 
C = p(:,t(3,:)); 
Ax = A(1,:);
Ay = A(2,:);
Bx = B(1,:);
By = B(2,:);
Cx = C(1,:);
Cy = C(2,:);
A2 = Ax.^2+Ay.^2;
B2 = Bx.^2+By.^2;
C2 = Cx.^2+Cy.^2;

%Circumcenter of the triangle (see Wikipedia for explanations of formulas)

Sx = (Ay.*(C2-B2) + By.*(A2-C2) + Cy.*(B2-A2))/2;
Sy = (Ax.*(B2-C2) + Bx.*(C2-A2) + Cx.*(A2-B2))/2;

a = (Ax.*(By-Cy) + Bx.*(Cy-Ay) + Cx.*(Ay-By));

G1 = Sx./a;
G2 = Sy./a;

CircumTri = [G1;G2];

% Barycenter of triangles

BaryTri = (A+B+C)/3;

% Diameter of triangle

DiamTri = sqrt(max([(Ax-Bx).^2+(Ay-By).^2; (Bx-Cx).^2+(By-Cy).^2; (Cx-Ax).^2+(Cy-Ay).^2],[], 1));


%Areas of triangles

Crossprod = (Bx-Ax).*(Cy-Ay)-(By-Ay).*(Cx-Ax);

if any(Crossprod==0)
    warning(['Flat triangle(s) detected, indices are:', num2str(find(Crossprod==0))])
end


AreaTri= abs((Bx-Ax).*(Cy-Ay)-(By-Ay).*(Cx-Ax))/2;

% Reordering vertices to get them in trigonometric order
t(2:3,Crossprod<0) = t(3:-1:2,Crossprod<0); 


% Filling the matrices with elements information

FVmsh.ContVols = t;
FVmsh.ContVolsInfo=[CircumTri;AreaTri;BaryTri;DiamTri];

%% Size of the mesh

FVmsh.Size = max(DiamTri);

%% Edges Matrix

D=zeros(4,round(1.8*nbtri));

% Creation of the "diamond" structure
% initialisation

D(1:2,1)=t([1 2],1);
D(3,1)=1;

D(1:2,2)=t([2 3],1);
D(3,2)=1;

D(1:2,3)=t([3 1],1);
D(3,3)=1;

nbedges=3;

%     For each triangle we consider the three edges
%     We test if it is already in D:
%     If it is not then we add it the triangle becomes its West triangle
%     If it is already the triangle becomes its East triangle

textprogressbar('Extracting edges: ');
for i=2:nbtri % loop on triangles
    for cpt = 1:3 % loop on edges of triangle
        edge=t([mod(cpt,3) mod(cpt+1,3)]+1,i);
        jtest1=find(D(1,1:nbedges)==edge(2));
        if (all(size(jtest1))==0)
            nbedges=nbedges+1;
            D(1:2,nbedges)=edge;
            D(3,nbedges)=i;
        else
            jtest2=find(D(2,jtest1)==edge(1));
            if (all(size(jtest2))==0)
                nbedges=nbedges+1;
                D(1:2,nbedges)=edge;
                D(3,nbedges)=i;
            else
                D(4,jtest1(jtest2))=i;
            end
        end
    end
    textprogressbar(100*(i-1)/nbtri);
end
D=D(:,1:nbedges);
FVmsh.Edges = [D;zeros(1,nbedges)];

textprogressbar('done')


%% EdgesInfo Matrix

disp('Computing edges geometrical information...')

%barycentre

edgesCenter=(p(1:2,D(1,:))+p(1:2,D(2,:)))/2;

%creation of vector SN, length and normal vector

SN=p(1:2,D(2,:))-p(1:2,D(1,:));
lengthSN=sqrt(SN(1,:).^2+SN(2,:).^2);
normalSN(1,:)=SN(2,:)./lengthSN;
normalSN(2,:)=-SN(1,:)./lengthSN;


%creation of E points

PointsE=zeros(2,nbedges);
interieur=find(D(4,:)>0);
bord=find(D(4,:)==0);
PointsE(1:2,interieur)=FVmsh.ContVolsInfo(1:2,D(4,interieur));
PointsE(1:2,bord) = edgesCenter(:,bord);

%creation of W points

PointsW=FVmsh.ContVolsInfo(1:2,D(3,:));

%creation of WE vectors and length

WE=PointsE-PointsW;
lengthWE=sqrt(WE(1,:).^2+WE(2,:).^2);


% diamond areas

SW=PointsW-p(1:2,D(1,:));
diamAreaW=1/2*abs(SW(1,:).*SN(2,:)-SW(2,:).*SN(1,:));

SE=PointsE-p(1:2,D(1,:));
diamAreaE=1/2*abs(SE(1,:).*SN(2,:)-SE(2,:).*SN(1,:));

FVmsh.EdgesInfo = [lengthSN;lengthWE;diamAreaW;diamAreaE;normalSN;edgesCenter];




%% Orthogonality test (always satisfied because of circumcenter definition)
disp('Checking admissibility and orthogonality of the mesh...')
tol = 1e-8;
FVmsh.Orthogonal = all(abs(dot(SN./lengthSN,WE./lengthWE,1))<tol);

%% Admissibility test 

FVmsh.Admissible = ~any(dot(WE./lengthWE, normalSN)<-tol);

if ~FVmsh.Admissible
    FVmsh.WhichNotAdmissible = find(dot(WE./lengthWE, normalSN)<-tol);
else
    FVmsh.WhichNotAdmissible = 0;
end

disp('FV mesh created.')

end
