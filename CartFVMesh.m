function [FEmsh, FVmsh] = CartFVMesh(xmin, xmax, ymin, ymax, Nx, Ny, varargin)
%CartFVMesh: Creates of a 2D quadrangular Cartesian "finite element"
%            mesh (with data similar to that of matlab pdetoolbox) and 
%            "finite volume" mesh (matlab structures containing the 
%            info needed to implement a TPFA finite volume scheme in 
%            2D.
% 
% [FEmsh, FVmsh] = CartesianEF(xmin, xmax, ymin, ymax, Nx, Ny)
% [FEmsh, FVmsh] = CartesianEF(xmin, xmax, ymin, ymax, Nx, Ny, 'xPeriodic')
% [FEmsh, FVmsh] = CartesianEF(xmin, xmax, ymin, ymax, Nx, Ny, 'yPeriodic')
% [FEmsh, FVmsh] = CartesianEF(xmin, xmax, ymin, ymax, Nx, Ny, 'xyPeriodic')
%
%%%%%%%%%
% INPUT
%%%%%%%%%
%
% xmin, xmax, ymin, ymax: Limits of the rectangular domain
%                         [xmin, xmax]\times[ymin, ymax]
%
% Nx, Ny: Number of cells in x and y directions
%
% Options: One can add 'xPeriodic', 'yPeriodic', 'xyPeriodic' to create a
%          mesh which is periodic in the x or y or both directions.
%
%%%%%%%%%
% OUTPUT
%%%%%%%%%
%
% FEmsh: matlab structure containing the Finite Element mesh. The fields of 
% FEmsh are the following.
%
% FEmsh.Vertices: Matrix containing the vertices in the mesh 
% (one vertex = one column)
%   Line 1: x coordinate
%   Line 2: y coordinate
%
% FEmsh.BoundaryEdges: Matrix containing the boundary edges of the mesh
% (one edge = one column)
%   Line 1: Endpoint 1 index (in FEmsh.Vertices)
%   Line 2: Endpoint 2 index (in FEmsh.Vertices)
%
% FEmsh.MaxNumEdges: Maximal number of edges of an element. 
%
% FEmsh.Elements: Matrix containing the elements 
% (one column = one element). 
%   Line 1: Vertex 1 index (in FEmsh.Vertices)
%   Line 2: Vertex 2 index (in FEmsh.Vertices)
%   ...
%   Line N: Vertex N index (in FEmsh.Vertices)
%   Line N+1: Index of the domain in which the element is (0 by default)
%
%%%%%%%%%%%%%%%
%
% FVmsh: Matlab structure containing the Finite Volume mesh. The fields of 
% FVmsh are the following.
%
% FVmsh.Vertices: Same as FEmsh.Vertices
%
% FVmsh.MaxNumEdges: Maximal number of edges of a control volume. 
%
% FVmsh.ContVols: Matrix containing the N-edges control volumes 
%                 of the mesh (one column = one control volume). The 
%                 vertices are reordered   in trigonometric order.
%   Line 1: Vertex 1 index (in FVmsh.Vertices)
%   Line 2: Vertex 2 index (in FVmsh.Vertices)
%   ...
%   Line N: Vertex N index (in FVmsh.Vertices)
%   Line N+1: Index of the domain in which the control volume is 
%             (0 by default)
%
% FVmsh.ContVolsInfo: Matrix containing additional geometric information 
%                     on the control volumes
%   Line 1: x coordinate of the center of the control volume (may not be
%           barycenter)
%   Line 2: y coordinate of the center of the control volume (may not be
%           barycenter)
%   Line 3: Area of the control volume
%   Line 4: x coordinate of the barycenter of the control volume
%   Line 5: y coordinate of the barycenter of the control volume
%   Line 6: Diameter of the control volume
%
%
%
% FVmsh.Edges: Matrix containing the edges of the mesh
%   (one edge = one column). Here is a schematic of an edge (vertical line)
%   W and E represent the neighboring triangles and S and N are the 
%   vertices at endpoints of the edge. By convention W is always inside the
%   domain and E does not exist if the edge is on the boundary.
%             N
%             |
%             |
%             |
%   W ------------------ E
%             |
%             |
%             |
%             S
%   Line 1: Index (in FEmsh.Vertices) of the South point
%   Line 2: Index (in FEmsh.Vertices) of the North point
%   Line 3: Index (in FEmsh.Elements / FVmsh.ContVols) of the West point
%   Line 4: Index (in FEmsh.Elements / FVmsh.ContVols) of the East point.
%           Set to 0 if the edge is on the boundary.
%   Line 5: Part of the boundary in which the edge is (0 by default)
%
% FVmsh.EdgesInfo: Matrix containing additional information on the edges.
%   Line 1: Length of the edge (South-North)
%   Line 2: Distance between the neighboring cell centers (East-West). If 
%           the edge is on the outside the distance is between West cell 
%           center and the edge barycenter.
%   Line 3: Area of the West diamond (triangle between edge and West cell
%           center).
%   Line 4: Area of the East diamond (triangle between edge and East cell
%           center). 0 if the edge is on the boundary.
%   Line 5: x coordinate of the normal vector to the edge pointing to the
%           East
%   Line 6: y coordinate of the normal vector to the edge pointing to the 
%           East
%   Line 7: x coordinate of the edge barycenter
%   Line 8: y coordinate of the edge barycenter
%
% FVmsh.Size: Maximum diameter of triangles
%
% FVmsh.Orthogonal: Boolean saying whether SN is orthogonal to WE for all
%                   control volumes.
%
% FVmsh.Admissible: Boolean saying whether the normal to SN pointing to the
%                   East is in the same direction as WE. For triangle 
%                   meshes it may fail if circumcenters are far from 
%                   triangles (i.e. take two neighboring flattened 
%                   triangles). Triangle meshes should be admissible if 
%                   they are produced with the Frontal Delaunay algorithm.
%
% FVmsh.WhichNotAdmissible: Contains the list of non-admissible edges (such
%                           as the normal to SN is not in the same 
%                           direction as WE). Contains 0 if admissible.


disp('-------------------------------')
disp('Creation of Cartesian FV mesh...')
%% Create vertices
xvec = linspace(xmin, xmax, Nx+1);
yvec = linspace(ymin, ymax, Ny+1);
dx = xvec(2)-xvec(1);
dy = yvec(2)-yvec(1);


% Vertices 
[Y,X] = meshgrid(yvec,xvec);
p = [reshape(X, 1, (Nx+1)*(Ny+1)); reshape(Y, 1, (Nx+1)*(Ny+1))];

%% Initialize FEmsh and FVmsh
FEmsh = struct;
FVmsh = struct;
FEmsh.Vertices = p;
FVmsh.Vertices = p;
FEmsh.MaxNumEdges = 4;
FVmsh.MaxNumEdges = 4;

%% Compute control volumes and edges

% Useful vectors
tmp = 1+(0:Nx) + (Nx+1)*(0:Ny)';
tmp = tmp(1:Ny, 1:Nx);
tmp = reshape(tmp',1,Nx*Ny);

tmp2 = 1+(0:Nx) + (Nx+1)*(0:Ny)';
tmp2 = tmp2(1:Ny,2:(Nx+1));
tmp2 = reshape(tmp2,1,Nx*Ny);

tmp3 = 1+(0:Nx-1) + Nx*(0:Ny-1)';
tmp4 = tmp3(1:Ny,2:Nx);
tmp3 = reshape(tmp3,1,Nx*Ny);
tmp4 = reshape(tmp4,1,(Nx-1)*Ny);

% Create the quadrangles
q = [tmp;tmp+1;tmp+Nx+2; tmp+Nx+1;0*tmp];
FVmsh.ContVols = q;
FEmsh.Elements = q;

% Create the horizontal edges
eHbeg = tmp(1:Nx);
eHmid = tmp(Nx+1:end);
eHend =  (Nx+1)*(Ny+1)-Nx+1:(Nx+1)*(Ny+1);
eH = [eHbeg, eHmid, eHend]; % South points
eH = [eH;[eHbeg+1, eHmid+1, eHend-1]]; % North points
eH = [eH;[1:Nx*Ny, Nx*Ny-Nx+1:Nx*Ny]]; % West element
eH = [eH;[eHbeg*0,1:Nx*(Ny-1), eHend*0]]; % East points


% Create the vertical edges
eVbeg = Nx+2:Nx+1:(Nx+1)*(Ny+1)-Nx;
eVmid = tmp2(1:(Nx-1)*Ny);
eVend = tmp2((Nx-1)*Ny+1:Nx*Ny);
eV = [eVbeg, eVmid, eVend];% South points
eV = [eV;[eVbeg-Nx-1, eVmid+Nx+1, eVend+Nx+1]];% North points
eV = [eV;[1:Nx:1+(Ny-1)*Nx, tmp3]];% West points
eV = [eV;[0*eVbeg, tmp4, 0*eVend]];% East points

% Regroup edges
e = [eH,eV];
nbedges = size(e,2);
e = [e;zeros(1,nbedges)];
FVmsh.Edges = e;

%% ElementsInfo

% Vertices of rectangles (ABCD) in the mesh

disp('Computing elements geometrical information...')

A = p(:,q(1,:)); 
B = p(:,q(2,:)); 
C = p(:,q(3,:)); 
D = p(:,q(4,:)); 

Center = (A+B+C+D)/4;
Area = 0*A + dx*dy;
Diam = 0*A +sqrt(dx*dx+dy*dy);

FVmsh.ContVolsInfo = [Center;Area;Center;Diam];

%% EdgesInfo Matrix

FVmsh.Size = sqrt(dx*dx+dy*dy);

disp('Computing edges geometrical information...')


%barycentre
edgesCenter=(p(1:2,e(1,:))+p(1:2,e(2,:)))/2;

% SN vectors, length and normal vector

SN=p(1:2,e(2,:))-p(1:2,e(1,:));
lengthSN=sqrt(SN(1,:).^2+SN(2,:).^2);
normalSN(1,:)=SN(2,:)./lengthSN;
normalSN(2,:)=-SN(1,:)./lengthSN;


%Points E

PointsE=zeros(2,nbedges);
interieur=find(e(4,:)>0);
bord=find(e(4,:)==0);
PointsE(1:2,interieur)=FVmsh.ContVolsInfo(1:2,e(4,interieur));
PointsE(1:2,bord) = edgesCenter(:,bord);

%Points W

PointsW=FVmsh.ContVolsInfo(1:2,e(3,:));

%WE vectors

WE=PointsE-PointsW;
lengthWE=sqrt(WE(1,:).^2+WE(2,:).^2);


% diamond areas

SW=PointsW-p(1:2,e(1,:));
diamAreaW=1/2*abs(SW(1,:).*SN(2,:)-SW(2,:).*SN(1,:));

SE=PointsE-p(1:2,e(1,:));
diamAreaE=1/2*abs(SE(1,:).*SN(2,:)-SE(2,:).*SN(1,:));

FVmsh.EdgesInfo = [lengthSN;lengthWE;diamAreaW;diamAreaE;normalSN;edgesCenter];




%% Orthogonality test (always satisfied because of circumcenter definition)
disp('Checking admissibility and orthogonality of the mesh...')
tol = 1e-8;
FVmsh.Orthogonal = all(abs(dot(SN./lengthSN,WE./lengthWE,1))<tol);

%% Admissibility test 

FVmsh.Admissible = ~any(dot(WE./lengthWE, normalSN)<-tol);

if ~FVmsh.Admissible
    FVmsh.WhichNotAdmissible = find(dot(WE./lengthWE, normalSN)<-tol);
else
    FVmsh.WhichNotAdmissible = 0;
end

disp('FV mesh created.')

end


