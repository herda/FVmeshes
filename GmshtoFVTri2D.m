function [FVmsh, FEmsh] = GmshtoFVTri2D(gmshSavefile)
% GmshToVF2D converts a triangular 2D mesh of gmsh into matlab
% structures containing the info needed to implement a TPFA finite 
% volume scheme in 2D.
%
% [VFmsh, FEmsh] = GmshtoFVTri2D(gmshSavefile)
%
%%%%%%%%%
% INPUT
%%%%%%%%%
%
% gmshSavefile : Save file (without the .m extension) produced by gmsh with
%                the option Mesh.Formart = 50 
%
%%%%%%%%%
% OUTPUT
%%%%%%%%%
%
% FEmsh: matlab structure containing the Finite Element mesh. The fields of 
% FEmsh are the following.
%
% FEmsh.Vertices: Matrix containing the vertices in the mesh 
% (one vertex = one column)
%   Line 1: x coordinate
%   Line 2: y coordinate
%
% FEmsh.BoundaryEdges: Matrix containing the boundary edges of the mesh
% (one edge = one column)
%   Line 1: Endpoint 1 index (in FEmsh.Vertices)
%   Line 2: Endpoint 2 index (in FEmsh.Vertices)
%
% FEmsh.MaxNumEdges: Maximal number of edges of an element. 
%
% FEmsh.Elements: Matrix containing the elements 
% (one column = one element). 
%   Line 1: Vertex 1 index (in FEmsh.Vertices)
%   Line 2: Vertex 2 index (in FEmsh.Vertices)
%   ...
%   Line N: Vertex N index (in FEmsh.Vertices)
%   Line N+1: Index of the domain in which the element is (0 by default)
%
%%%%%%%%%%%%%%%
%
% FVmsh: Matlab structure containing the Finite Volume mesh. The fields of 
% FVmsh are the following.
%
% FVmsh.Vertices: Same as FEmsh.Vertices
%
% FVmsh.MaxNumEdges: Maximal number of edges of a control volume. 
%
% FVmsh.ContVols: Matrix containing the N-edges control volumes 
%                 of the mesh (one column = one control volume). The 
%                 vertices are reordered   in trigonometric order.
%   Line 1: Vertex 1 index (in FVmsh.Vertices)
%   Line 2: Vertex 2 index (in FVmsh.Vertices)
%   ...
%   Line N: Vertex N index (in FVmsh.Vertices)
%   Line N+1: Index of the domain in which the control volume is 
%             (0 by default)
%
% FVmsh.ContVolsInfo: Matrix containing additional geometric information 
%                     on the control volumes
%   Line 1: x coordinate of the center of the control volume (may not be
%           barycenter)
%   Line 2: y coordinate of the center of the control volume (may not be
%           barycenter)
%   Line 3: Area of the control volume
%   Line 4: x coordinate of the barycenter of the control volume
%   Line 5: y coordinate of the barycenter of the control volume
%   Line 6: Diameter of the control volume
%
%
%
% FVmsh.Edges: Matrix containing the edges of the mesh
%   (one edge = one column). Here is a schematic of an edge (vertical line)
%   W and E represent the neighboring triangles and S and N are the 
%   vertices at endpoints of the edge. By convention W is always inside the
%   domain and E does not exist if the edge is on the boundary.
%             N
%             |
%             |
%             |
%   W ------------------ E
%             |
%             |
%             |
%             S
%   Line 1: Index (in FEmsh.Vertices) of the South point
%   Line 2: Index (in FEmsh.Vertices) of the North point
%   Line 3: Index (in FEmsh.Elements / FVmsh.ContVols) of the West point
%   Line 4: Index (in FEmsh.Elements / FVmsh.ContVols) of the East point.
%           Set to 0 if the edge is on the boundary.
%   Line 5: Part of the boundary in which the edge is (0 by default)
%
% FVmsh.EdgesInfo: Matrix containing additional information on the edges.
%   Line 1: Length of the edge (South-North)
%   Line 2: Distance between the neighboring cell centers (East-West). If 
%           the edge is on the outside the distance is between West cell 
%           center and the edge barycenter.
%   Line 3: Area of the West diamond (triangle between edge and West cell
%           center).
%   Line 4: Area of the East diamond (triangle between edge and East cell
%           center). 0 if the edge is on the boundary.
%   Line 5: x coordinate of the normal vector to the edge pointing to the
%           East
%   Line 6: y coordinate of the normal vector to the edge pointing to the 
%           East
%   Line 7: x coordinate of the edge barycenter
%   Line 8: y coordinate of the edge barycenter
%
% FVmsh.Size: Maximum diameter of triangles
%
% FVmsh.Orthogonal: Boolean saying whether SN is orthogonal to WE for all
%                   control volumes.
%
% FVmsh.Admissible: Boolean saying whether the normal to SN pointing to the
%                   East is in the same direction as WE. For triangle 
%                   meshes it may fail if circumcenters are far from 
%                   triangles (i.e. take two neighboring flattened 
%                   triangles). Triangle meshes should be admissible if 
%                   they are produced with the Frontal Delaunay algorithm.
%
% FVmsh.WhichNotAdmissible: Contains the list of non-admissible edges (such
%                           as the normal to SN is not in the same 
%                           direction as WE). Contains 0 if admissible.


%% Extract data 

disp('-------------------------------')
disp(['Extracting data from gmsh file ''', gmshSavefile,''''])
run(gmshSavefile)

p = msh.POS(:,1:2)';
e = msh.LINES';
t = msh.TRIANGLES';


%% Create Finite Element mesh
disp('-------------------------------')
disp('Creating FE mesh...')
FEmsh = struct;
FEmsh.Vertices = p;
FEmsh.BoundaryEdges = e;
FEmsh.MaxNumEdges = 3;
FEmsh.Elements = t;
disp('FE mesh created.')
%% Create Finite Volume mesh

FVmsh=FEtoFVtri(FEmsh);

disp('-------------------------------')
disp('Saving FE and FV meshes in file...')
save(gmshSavefile, 'FEmsh', 'FVmsh');  
disp('Done.')
end


